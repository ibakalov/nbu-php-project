<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;


class AuthController extends Controller
{
    public function register(Request $request): RedirectResponse
    {
        $request->validate([
            'email' => 'required|email:filter|unique:user|max:50',
            'password' => 'required|max:50|confirmed',
        ]);

        // Create a new user
        $user = User::create([
            'full_name' => $request->full_name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        auth()->login($user);

        return redirect('/')->with('success', 'Registration successful');
    }

    public function login(Request $request): RedirectResponse|View
    {
        $request->validate([
            'email' => 'required|exists:user',
            'password' => 'required|string',
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return redirect('/')->with('success', 'Login successful');
        } else {

            return view('login', ['email' => $request->email, 'pass_error' => 'Wrong password']);
        }
    }

    public function logout(): RedirectResponse
    {
        Auth::logout();
        return redirect('/login')->with('success', 'Logout successful');
    }
}