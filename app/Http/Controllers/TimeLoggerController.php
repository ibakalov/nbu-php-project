<?php

namespace App\Http\Controllers;

use App\Models\LoggedTime;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\View\View;

const ERROR_CODES = [
    '23000' => 'There is already a saved row with one of the provided dates'
];

class TimeLoggerController extends Controller
{
    public function log_time(Request $request) {

        if (!$this->is_current_years($request->from_date)) {
            return Response::json([
                'message' => 'The from date is not in the current or last year'
            ], 500);
        }

        if ($this->is_to_date_is_earlier_than_from($request->to_date, $request->from_date)) {
            return Response::json([
                'message' => 'The to date is earlier than the from date'
            ], 500);
        }


        $request->validate([
            'from_date' => 'required',
            'to_date' => 'required',
            'time' => 'required',
        ]);

        $user = Auth::user();
        $data = [];


        $dates = $this->get_dates_between($request->from_date, $request->to_date);


        foreach ($dates as $date) {
            $data[] = [
                'date' => $date,
                'time' => $request->time,
                'user_id' => $user->id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
        }

        try {
            LoggedTime::insert($data);
            $request->session()->flash('success', 'Date(s) logged successful');

            return Response::json([
                'message' => 'Date(s) logged successful'
            ]);

        } catch (\Exception $e) {

            return Response::json([
                'message' => $this->error_codes($e)
            ], 500);
        }
    }

    public function get_table(Request $request): View
    {
        $user = Auth::user();
        $sort = request('order') ? request('order') : 'desc';
        $by = request('by') ? request('by') : 'date';

        $logged_time = LoggedTime::where('user_id', $user->id)->orderBy($by, $sort)->paginate(5);


        return view('home', ['logged_time' => $logged_time]);
    }

    public function delete_single($id): RedirectResponse
    {

        try {
            $logged_time = LoggedTime::find($id);
            $logged_time->delete();

            return redirect('/')->with('success', 'Date deleted successful');
        } catch (\Exception $e) {
            return redirect('/')->with('error', $this->error_codes($e));
        }


    }

    private function get_dates_between($from_date, $to_date): array
    {
        $start = new DateTime($from_date);
        $end = new DateTime($to_date);

        $dateInterval = new DateInterval('P1D'); // 1 day interval
        $dateRange = new DatePeriod($start, $dateInterval, $end);

        $dates = [];
        foreach ($dateRange as $date) {
            $dates[] = $date->format('Y-m-d');
        }
        $dates[] = $end->format('Y-m-d');

        return $dates;
    }

    private function error_codes($error): string
    {
        if (ERROR_CODES[$error->getCode()]) {

            return ERROR_CODES[$error->getCode()];
        } else {
            return 'The dates are not inserted';
        }
    }

    private function is_current_years($date): bool
    {
        $current_year = date('Y');

        return date('Y', strtotime($date)) >= $current_year - 1;
    }

    private function is_to_date_is_earlier_than_from($date_to, $date_from): bool
    {
        return date('Y-m-d', strtotime($date_to)) < date('Y-m-d', strtotime($date_from));
    }

}