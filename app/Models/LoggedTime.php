<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class LoggedTime extends Model
{
//    use HasApiTokens, HasFactory, Notifiable;
//
//    use HasFactory;

    public $table = 'logged_time';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'from_date',
        'to_date',
        'time',
    ];
}
