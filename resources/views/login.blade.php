@extends('layout')
@section('title', 'Login')
@section('content')

    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <div class="d-flex flex-justify-center flex-align-center h-vh-100">
        <form id="form-login" action="/login" method="post">
            @csrf
            <h3 class="mb-1">Please login</h3>
            <div class="pos-relative">
            <input required type="email" data-role="materialinput"
                   name="email"
                   value="{{ old('email') ? old('email') : (isset($email) ? $email : '')  }}"
                   class="mail-input mt-2"
                   data-icon="<span class='mif-envelop'>"
                   data-label="Email"
                   data-form-name="email"
                   data-informer="Enter an email address"
                   data-validate="required email"
                   placeholder="Enter an email address"
            >
             @error('email')
             <div class="alert alert-danger">{{ $message }}</div>
             @enderror
            </div>
            <div class="pos-relative">
            <input required type="password" data-role="materialinput"
                   name="password"
                   value="{{ old('password') }}"
                   class="password-input  mt-2"
                   data-icon="<span class='mif-key'>"
                   data-label="Password"
                   data-form-name="password"
                   data-informer="Enter your password"
                   data-validate="required password"
                   placeholder="Enter your password"
            >
            @error('password')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
                @if(isset($pass_error))
                    <div class="alert alert-danger">{{ $pass_error }}</div>
                @endif
            </div>

            <div class="d-flex button-wrapper flex-align-center w-100 mt-4">
                <button class="button primary rounded">Submit</button>
                <span class="ml-4">You don't have an account <a href="/register">register</a></span>
            </div>
        </form>
    </div>

@endsection