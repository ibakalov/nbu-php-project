<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @vite(['resources/css/app.scss', 'resources/js/app.js'])
    <title>@yield('title')</title></head>
<body>
<div class="container">
    @if(Auth::check())
    <div class="header w-100 h-25 d-flex flex-align-center pt-2 pb-2 inherit-background">
        <a href="/logout" class="ml-auto mr-4 button primary rounded">Logout</a>
    </div>
    @endif

    @if (Session::has('success'))
        <script>
            setTimeout(function() {
                tostify('{{ Session::get('success') }}', 'var(--success)')
            }, 400);
        </script>
    @endif
    @if (Session::has('error'))
        <script>
            setTimeout(function() {
                tostify('{{ Session::get('error') }}', 'var(--error)')
            }, 400);
        </script>
    @endif

    @yield('content')
</div>
</body>
</html>