@extends('layout')
@section('title', 'Log Time')
@section('content')
    <div class="d-flex mb-5 flex-align-center">
            <h3 class="mb-1">Time logger</h3>
                <button type="button" class="ml-4 button primary rounded openLogTimeModal">Log time</button>
    </div>
       <div>
           @if ($logged_time->count() > 0)
               <table class="table"
                      data-role="table"
                      data-source="data/table.json"
                      data-show-rows-steps="false"
                      data-show-search="false"
                      data-show-pagination="true"
               >
                   <thead>
                   <tr>
                       <th data-name="date" class="sortable-column sort-{{ Request::get('order') }}" ><a href="/?by=date&order={{ Request::get('order') == 'desc' || !Request::get('order') ? 'asc' : 'desc' }}">Date</a></th>
                       <th data-name="time" >Logged time</th>
                       <th></th>
                   </tr>
                   </thead>
                   <tbody>
                   @foreach($logged_time->toArray()["data"] as $index => $time)
                       <tr>
                           <td>{{$time['date']}}</td>
                           <td>
                               {{ (new DateTime($time['time']))->format('H') }} hour(s)
                               {{ (new DateTime($time['time']))->format('i') }} minute(s)
                           </td>
                           <td><a href="delete-single/{{$time['id']}}">Delete</a></td>
                       </tr>
                   @endforeach
                   </tbody>
               </table>
               <div class="pagination d-flex flex-align-center">
                   {{ $logged_time->links() }}
               </div>

               @else

                <div class="d-flex flex-align-center">
                     <h5 class="mb-1 mt-10 text-center">No time logged yet</h5>
           @endif
       </div>

@endsection