import './bootstrap';
import 'metro4/source/core';
import 'metro4/source/form.js';
import 'metro4/source/components/dialog';
import 'metro4/source/components/colors';

import Toastify from "toastify-js";
import {http} from "./http.js";



document.addEventListener("DOMContentLoaded", function(event) {

 document.querySelector('.openLogTimeModal')?.addEventListener('click', () => {
     const date = new Date();
     const maxInitialDate = `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}`;

     Metro.dialog.create({
         title: "Log time",
         content: `<div>
                    <form id="log-time-form" action="/time-logger" method="post">
                        <input name="_token" value="${document.querySelector('[name="csrf-token"]').content}" type="hidden">
                        <div class="date-inputs second-date mt-2">
                            <label for="from-date">Date from</label>
                            <input required class="w-100 d-block mt-1 pt-1 pb-1" type="date" id="from-date" name="from_date" value="${maxInitialDate}" max="${maxInitialDate}" />
                        </div>
                        
                        <div class="date-inputs second-date mt-2">
                            <label for="to-date">Date to</label>
                            <input required class="w-100 d-block mt-1 pt-1 pb-1" type="date" id="to-date" name="to_date" value="${maxInitialDate}" max="${maxInitialDate}" />
                        </div>
                        
                         <div class="date-inputs time mt-2">
                            <label for="time">Time</label>
                            <input required class="w-100 d-block mt-1 pt-1 pb-1" min="1" type="time" id="time" name="time"  />
                        </div>
                    </form>
                </div>`,
         closeButton: true,
         closeAction: false,
         actions: [
             {
                 caption: "Ok",
                 onclick: function(event){
                     const form = document.getElementById('log-time-form');
                        if (form.checkValidity()) {
                            const from_date = document.getElementById('from-date').value;
                            const to_date = document.getElementById('to-date').value;
                            const time = document.getElementById('time').value;

                            http(`api/log-time`, "POST", {from_date,to_date, time})
                                .then((response) => {
                                    if (response) {
                                        location.reload();
                                    }
                                })
                        } else {
                            form.reportValidity();
                        }
                 }
             }
         ],
         onOpen: function(){
             const date = new Date();
             let day = date.getDate();
             let month = date.getMonth() + 1;
             let year = date.getFullYear();

             let currentDate = new Date().toJSON().slice(0, 10);

             document.getElementById('from-date').setAttribute('max', `${currentDate}`);
             document.getElementById('to-date').setAttribute('max', `${currentDate}`);

             document.getElementById('from-date').addEventListener('change', setMinDate)
             document.getElementsByClassName('js-dialog-close')[0].addEventListener('click', closeModal)
         },
         onClose: function(){
             document.getElementById('from-date').removeEventListener('change', setMinDate)
             document.getElementsByClassName('js-dialog-close')[0].removeEventListener('click', closeModal)
         }
     });
 })
})


function setMinDate() {
    const toDate = document.getElementById('to-date');
    toDate.setAttribute('min', this.value);
}


function tostify(msg, color = "var(--error)") {
    Toastify({
        text: msg,
        duration: 3000,
        newWindow: true,
        close: true,
        gravity: "top", // `top` or `bottom`
        position: "right", // `left`, `center` or `right`
        stopOnFocus: true, // Prevents dismissing of toast on hover
        style: {
            background: color,
        },
        onClick: function () {}, // Callback after click
    }).showToast();
}

function closeModal() {
    Metro.dialog.close('.dialog')
}

window.tostify = tostify;