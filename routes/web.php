<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\TimeLoggerController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('login', function () {
    return view('login');
})->name('login');

Route::post('/login', [AuthController::class, 'login']);

Route::get('/register', function () {
    return view('register');
})->name('register');

Route::post('/register', [AuthController::class, 'register']);

Route::get('/', [TimeLoggerController::class, 'get_table'])->middleware('auth');

Route::get('/logout', [AuthController::class, 'logout'])->middleware('auth');

Route::get('/delete-single/{id}', [TimeLoggerController::class, 'delete_single'])->middleware('auth');


